/**
 * Rectangle.java / GeometryCalculator
 * Defines the Rectangle class and declares methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/19/2017
 */

public class Rectangle extends Shape{

	public Rectangle(double width, double height) {	//Rectangles have only two side lengths
		this.sideLengths = new double[] {width, width, height, height};
	}

	public double getArea() {
		this.area = (this.sideLengths[0] * this.sideLengths[1]);	// Area of a rectangle
		return this.area;
	}

}
