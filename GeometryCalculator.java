/**
 * GeometryCalculator.java / GeometryCalculator
 * Calculates area and perimeter of circles, squares, rectangles, triangles, regular pentagons
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/21/2017
 */

import java.util.Scanner;

public class GeometryCalculator {
	
	static boolean continueRunningProgram = true;
	
	private static Scanner keyReader = new Scanner(System.in);
	
	static Shape shapeBeingAnalyzed;	//static so these may be referenced from within static classes
	static double[] shapeSideLengths;
	
	public static void main(String[] args) {
		showAboutText();
		showHelpText();
		
		while(continueRunningProgram) {
			System.out.println("Enter command ('help' for help): ");
			String shapeTypeSelected = keyReader.nextLine();
			
			if(getShapeType(shapeTypeSelected)) {	//Only show this if user actually entered a real shape
				System.out.println("Analysis of " + shapeBeingAnalyzed.shapeName + ":");
				System.out.println("Perimeter:     " + shapeBeingAnalyzed.getPerimeter() + " units");
				System.out.println("Area:          " + shapeBeingAnalyzed.getArea() + " sq. units");
			}
		}
		
		System.out.println("Exiting program.");
	}
	
	//getShapeType returns true if a real shape was selected, false for anything else
	public static boolean getShapeType(String stringToParse) {	
		if (stringToParse.equalsIgnoreCase("help")) {
			showHelpText();
			return false;
			
		} else if(stringToParse.equalsIgnoreCase("about")) {
			showAboutText();
			return false;
			
		} else if(stringToParse.equalsIgnoreCase("circle")) {
			shapeSideLengths = getSideLengths(1);	//Get the lengths of the shape's sides from user
			shapeBeingAnalyzed = new Circle(shapeSideLengths[0]);		//Define shape with this info
			return true;
			
		} else if(stringToParse.equalsIgnoreCase("rectangle")) {
			shapeSideLengths = getSideLengths(2);
			shapeBeingAnalyzed = new Rectangle(shapeSideLengths[0], shapeSideLengths[1]);
			return true;
			
		} else if(stringToParse.equalsIgnoreCase("regpentagon")) {
			shapeSideLengths = getSideLengths(1);
			shapeBeingAnalyzed = new RegularPentagon(shapeSideLengths[0]);
			return true;
			
		} else if(stringToParse.equalsIgnoreCase("square")) {
			shapeSideLengths = getSideLengths(1);
			shapeBeingAnalyzed = new Square(shapeSideLengths[0]);
			return true;
			
		} else if(stringToParse.equalsIgnoreCase("triangle")) {
			shapeSideLengths = getSideLengths(3);
			shapeBeingAnalyzed = new Triangle(shapeSideLengths[0], shapeSideLengths[1], shapeSideLengths[2]);
			if(isRealTriangle(shapeBeingAnalyzed)) {
				return true;
			} else {
				System.out.println("Error: This program cannot handle polygons on imaginary plane.");
				return false;
			}
			
		} else if(stringToParse.equalsIgnoreCase("exit")) {
			continueRunningProgram = false;
			return false;
					
		} else {
			System.out.println("Invalid input. Enter 'help' to display the help.");
			return false;
		}
	}
	
	private static boolean isRealTriangle(Shape triangleToTest) {	//Determine if the triangle is real
		if(Double.isNaN(triangleToTest.getArea())) {	
			//If Heron's formula returns an imaginary value, we know the triangle cannot exist													
			return false;
		} else {
			return true;
		}
	}
	
	public static double[] getSideLengths(int numberOfSidesToGet) {
		double[] sideLengthsEntered = new double[numberOfSidesToGet]; //Keep a list of sides entered
		
		//Used "iteration" instead of standard "i" for readability and to demonstrate understanding
		for(int iteration = 0; iteration < numberOfSidesToGet; iteration++) {	 //Loop once for ea. side
			System.out.println("Enter length of side " + (iteration + 1) + ": ");
			
			double sideLengthEntered = 0;
			String userInputValue = keyReader.nextLine();
				//nextLine was used instead of nextDouble due to the Scanner
				//being a line behind when returning to the primary routine
			
			try {
				sideLengthEntered = Double.parseDouble(userInputValue);
			} catch(NumberFormatException exception) {	//Make sure the user actually entered a number
				System.out.println("Error: Value entered was not a number. Changed to 1.0.");
			}
			
			if(sideLengthEntered == 0) {
				System.out.println("This program cannot handle shapes on imaginary plane. Changed to 1.0.");
				sideLengthEntered = 1;
			}
			
			if(sideLengthEntered < 0) {
				System.out.println("This program cannot handle shapes on imaginary plane. Sign flipped.");
				sideLengthEntered = Math.abs(sideLengthEntered);
			}
			sideLengthsEntered[iteration] = sideLengthEntered;
								
		}
		
		return sideLengthsEntered;
	}
	
	public static void showAboutText() {		//Provides information about the program
		System.out.println("            Basic Geometry Calculator (UNITLESS)            ");
		System.out.println(" This program finds areas and perimeters of various shapes. ");
		System.out.println("     For usage information, enter 'help' at the console.    ");
		System.out.println("------------------------------------------------------------");
	}
	
	public static void showHelpText() {		//Shows information regarding program usage
		System.out.println();
		System.out.println("At the console, enter any of the following to select a function:");
		System.out.println("about        Displays information about the program and its usage.");
		System.out.println("circle       Analyzes a circle based on its radius.");
		System.out.println("exit         Exits the program.");
		System.out.println("help         Displays this dialogue.");
		System.out.println("rectangle    Analyzes a rectangle based on its height and width.");
		System.out.println("regpentagon  Analyzes a regular pentagon based on one side length.");
		System.out.println("square       Analyzes a square based on one side length.");
		System.out.println("triangle     Analyzes a triangle based on three side lengths.");
	}
}
