/**
 * RegularPentagon.java / GeometryCalculator
 * Defines the regular pentagon class and declares methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/21/2017
 */

public class RegularPentagon extends Shape{

	public RegularPentagon(double newSideLength) {	//The "sideLength" of a square is the same for all sides, we only need one
		this.sideLengths = new double[] {newSideLength, newSideLength, newSideLength, newSideLength, newSideLength};
	}

	public double getArea() {
		double radius = ((this.sideLengths[0] * Math.sin((54*Math.PI)/180))) / (Math.sin((72*Math.PI)/180) );
		
		Triangle insideTriangle = new Triangle(sideLengths[0], radius, radius);
		this.area = (5 * insideTriangle.getArea());

		return this.area;
	}

}
