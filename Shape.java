/**
 * Shape.java / GeometryCalculator
 * Abstraction for shape classes - define what a shape should consist of, set up abstract methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/19/2017
 */

public abstract class Shape {
	
	protected double numberOfSides;
	
	public double sideLengths[];	// Contains the lengths of sides. Array used to allow for variable size, accounts for diff. shapes
	protected double	 perimeter;		// Protected – only used within class by getPerimeter()
	protected double area;			// Only used within class by getArea()
	
	public double getPerimeter() {	// These methods should be present for all shape types. Calculate and return perimeter
		double sumOfSideLengths = 0;
		for(int i = 0; i < sideLengths.length; i++) {
			sumOfSideLengths = sumOfSideLengths + sideLengths[i];
		}
		return sumOfSideLengths;
	};
	
	public abstract double getArea();	// Calculate and return area

}
