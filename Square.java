/**
 * Square.java / GeometryCalculator
 * Defines the Square class and declares methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/19/2017
 */

public class Square extends Shape{

	public Square(double newSideLength) {	//The "sideLength" of a square is the same for all sides, we only need one
		this.sideLengths = new double[] {newSideLength, newSideLength, newSideLength, newSideLength};
	}

	public double getArea() {
		this.area = (this.sideLengths[0] * this.sideLengths[0]);	// Area of a square
		return this.area;
	}

}
