/**
 * Triangle.java / GeometryCalculator
 * Defines the Triangle class and declares methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/19/2017
 */

public class Triangle extends Shape{

	public Triangle(double firstSide, double secondSide, double thirdSide) {	//Triangles have three sides
		this.sideLengths = new double[] {firstSide, secondSide, thirdSide};
	}

	public double getArea() {						// Area of any triangle using Heron's formula
		double halfPerim = this.getPerimeter() / 2;	// Store half the perimeter as a variable, used heavily in area formula
		this.area = ( Math.sqrt(halfPerim  * (halfPerim - this.sideLengths[0])  * 
								(halfPerim - this.sideLengths[1])  * (halfPerim - this.sideLengths[2])));	
		return this.area;
	}

}
