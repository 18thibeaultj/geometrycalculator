/**
 * Circle.java / GeometryCalculator
 * Defines the Circle class and declares methods
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/19/2017
 */

public class Circle extends Shape {

	public Circle(double radius) {	//The "sideLength" of a circle is its radius, so we only need one side
		this.sideLengths = new double[] {radius};
	}

	public double getPerimeter() {	//Circumference, in context of a circle
		this.perimeter = (2 * this.sideLengths[0] * Math.PI);		// "Perimeter" of a circle
		return this.perimeter;
	}

	public double getArea() {
		this.area = (this.sideLengths[0] * this.sideLengths[0] * Math.PI);	// Area of a circle
		return this.area;
	}

}
